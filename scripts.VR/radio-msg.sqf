/*
Radio message with a custom callsign
Sends a customized message to the unit, with a customized callsign
Simply place a trigger with the "player" and "present" (or as you need it).
Then in the trigger activation write this function:
*/

 [variable] sideChat "[message]";
