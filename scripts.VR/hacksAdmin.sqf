//Unlimited Ammo:
//Gives a vehicle or player unlimted ammo.
this addEventHandler ["Fired",{(_this select 0) setVehicleAmmo 1}];

//Unlimted Ammo with delay:
//40 in this case is how often it triggers the rearm. (Seconds)
nul = this spawn { while {alive _this} do { _this setVehicleAmmo 1; sleep 40 } };

//Change texture on a flag:
//This can be done in both jpg or paa. (1024x512 generally works)
this SetFlagTexture "images\imagename.jpg";

//Set Damage:
//1 = destroyed/dead
this setdammage 0.5;

//No Surrender:
//Forces a AI to fight, aka doesnt allow it to flee.
this allowFleeing 0;

//Disable/Enable Damage:
//Can be done easily in Eden, but you never know when you need this.
this allowdammage True/False;

//Turn On/Off lights on vehicle.
//Turns on and off the lights of a vehicle.
this Switchlight true/false;

//Earthquake:
//Triggers a Earthquake:
[1] call BIS_fnc_earthquake;

Skip time:
Skips x hours.
skipTime 4;

//Engine On/Off:
//Turn on or off a engine.
vehicleName engineOn false/true;
