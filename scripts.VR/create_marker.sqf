// Trigger for create a marker - Also creates a hint

hint "An S.O.S Signal Was Detected";
_marker1 = createMarker ["markerSOS", [2188, 5362]];
"markerSOS" setMarkerShape "Ellipse";
"markerSOS" setMarkerSize [50, 50];
"markerSOS" setMarkerBrush "DIAGGRID";
"markerSOS" setMarkerColor "ColorYellow";
