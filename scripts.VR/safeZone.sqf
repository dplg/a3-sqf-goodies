/*
safeZone

Setup: Place in the init
[] execVM "scripts\safeZone.sqf";

Add marker with unique name and specify the distance for safety_zones

Example: [["marker1", radius1], ["marker2", radius2], ...]
*/



if (isServer) exitWith {};


#define SAFETY_ZONES    [["safe", 300]]
#define MESSAGE "WTH! Stop shooting or throwing grenades! You are in a safe zone!"

     if (isDedicated) exitWith {};
     waitUntil {!isNull player};

switch (playerSide) do
{
    case west:
    {


     player addEventHandler ["Fired", {
            if ({(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) then
            {
             deleteVehicle (_this select 6);
             titleText [MESSAGE, "PLAIN", 3];
             };
        }];

    };


};
