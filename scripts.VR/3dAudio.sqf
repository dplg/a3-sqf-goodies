// Plays 3D audio on the given object, this one can be tricky sometimes when it comes to volume. (smallRadio = the object)
soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;
soundToPlay = soundPath + "sounds\radio_1.ogg";
playSound3D [soundToPlay, smallRadio, false, getPosASL smallRadio, 8, 1, 60];
