//Only fire a trigger when Multiple Triggers Have fired
triggerActivated TR_KILL_Parent_SU AND triggerActivated TR_Destroy_Supplies_Parent_SU;


//Fire a trigger when either Triggers Have fired
triggerActivated TR_KILL_Parent_SU OR triggerActivated TR_Destroy_Supplies_Parent_SU;
