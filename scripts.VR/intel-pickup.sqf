//Add Action to Pickup Intelligence, and Delete on Pickup

// Add Action to Pickup Intelligence, and Delete on Pickup
this addAction["Take TOR Connected Electronic Device", {deleteVehicle (_this select 0);
// NAME THE ITEM
player linkitem "ItemName"; hint "TOR Devices Picked Up!"}];
// CALLS
systemChat "An electronic device containing enemy defense locations was just picked up, check your map.";
if (isServer) then {
this setVariable ["RscAttributeDiaryRecord_texture","a3\structures_f_epc\Items\Documents\Data\document_secret_01_co.paa", true];
[this,"RscAttributeDiaryRecord",["Intel Picked Up","Sweet Intel, these positions mark enemy defenses.",""]] call bis_fnc_setServerVariable;
this setVariable ["recipients", west, true];
[this,"init"] spawn bis_fnc_initIntelObject;
};
