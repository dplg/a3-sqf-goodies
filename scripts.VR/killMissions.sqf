//
//Kill a single ennemy/destroy a thing: triggers condition, write
!alive [variable name of the thing you want dead]

//if you want multiple things dead, like 5 men, write
!alive [name] && !alive [name]

//Clear an area of ennemies:
//create a trigger of the desired size.
//Set it to "opfor", "not present".
//replace "this" by
count thislist < [max enemies to count]
