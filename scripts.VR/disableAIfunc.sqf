// Disable Ai Functions

// If placed directly on ai then use "this" otherwise use the variable name, prevents a Ai from doing the defined action.
this disableAI "PutActionHere";
// example...
this disableAI "Move";
/*
    "TARGET" - stop the unit to watch the assigned target / group commander may not assign targets.
    "AUTOTARGET" - prevent the unit from assigning a target independently and watching unknown objects.
    "MOVE" - disable the AI's movement. (Allows it to still move it's head & body but not move)
    "ANIM" - disable ability of AI to change animation.
    "TEAMSWITCH" - AI disabled because of Team Switch.
    "WEAPONAIM" - no weapon aiming.
    "AIMINGERROR" - prevents AI's aiming from being distracted by its shooting, moving, turning, reloading, hit, injury, fatigue, etc.
    "SUPPRESSION" - prevents AI from being suppressed.
    "CHECKVISIBLE" - disables visibility raycasts.
    "COVER" - disables usage of cover positions by the AI.
    "AUTOCOMBAT" - disables autonomous switching to COMBAT when in danger.
    "PATH" - stops the AI’s movement but not the target alignment.
    "MINEDETECTION" - disable Ai mine detection.
    "ALL" - all of the above. (At this point you might as well turn off Simulation in Eden)
    */
